__author__ = 'Bohdan'
import pygame
import os
import sys
from Button import *
from Settings import *
import MenuScreen
from GameScreen import *

pygame.init()
os.environ['SDL_VIDEO_CENTERED'] = '1'

class SettingsScreen(object):

    def __init__(self):
        print("Settings Start")
        self.__width = 800
        self.__height = 600
        self.__screen = pygame.display.set_mode((self.__width, self.__height))
        pygame.display.set_caption("Settings")
        self.__menu = Button()

    def sett_label(self):
        basicfont = pygame.font.SysFont(None, 100)
        text = basicfont.render('Settings', True, Colors.bord)
        textrect = text.get_rect()
        textrect.centerx = self.__screen.get_rect().centerx
        textrect.centery = self.__screen.get_rect().centery - self.__height/3
        self.__screen.blit(text, textrect)

    def update_buttons(self, btn_width, btn_height, shift_x, shift_y, color, key):
        if key == 1: self.__menu.create_button(self.__screen, color,
                                   30 - shift_x, self.__height-100-shift_y,
                                   btn_height, btn_width, 0, "Back", (255, 255, 255),2)
        elif key == 2:
            print('2')
        self.sett_label()

    def settings(self):
        time = pygame.time.Clock()
        life_cicle = True
        while life_cicle:
            self.__screen.fill(Colors.blue)
            self.update_buttons(50, 100, 0, 0, Colors.green, 1)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    life_cicle = False
                    break
                elif event.type == MOUSEBUTTONDOWN:
                    if self.__menu.hover(pygame.mouse.get_pos()):
                        del self
                        MenuScreen.MenuScreen().menu()

            if self.__menu.hover(pygame.mouse.get_pos()):
                self.update_buttons(60, 120, 10, 10, Colors.yellow, 1)
                time.tick(24)

            pygame.display.update()
        pygame.quit()
        quit()