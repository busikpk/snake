__author__ = 'Bohdan'
import pygame
from Button import *
from pygame.locals import *
import os
from GameScreen import GameScreen
from Settings import *
from SettingsScreen import *

pygame.init()
os.environ['SDL_VIDEO_CENTERED'] = '1'

class MenuScreen(object):

    def __init__(self):
        print('Menu')
        self.__start = Button()
        self.__settings = Button()
        self.__width = 800
        self.__height = 600
        self.__screen = pygame.display.set_mode((self.__width, self.__height), 0, 32)
        pygame.display.set_caption("Snake")

    def snake_label(self):
        basicfont = pygame.font.SysFont(None, 100)
        text = basicfont.render('Snake', True, Colors.bord)
        textrect = text.get_rect()
        textrect.centerx = self.__screen.get_rect().centerx
        textrect.centery = self.__screen.get_rect().centery - self.__height/3
        self.__screen.blit(text, textrect)

    def update_buttons(self, btn_width, btn_height, shift_x, shift_y, color, key):
        if key == 1: self.__start.create_button(self.__screen, color,
                                   self.__width/2-100-shift_x, self.__height/2-100-shift_y,
                                   btn_height, btn_width, 0, "Start Game", (255, 255, 255),2)
        elif key == 2: self.__settings.create_button(self.__screen, color,
                                   self.__width/2-100-shift_x, self.__height/2+50-shift_y,
                                   btn_height, btn_width, 0, "Settings", (255, 255, 255),2)
        self.snake_label()

    def menu(self):
        pygame.init()
        pygame.display.update()
        time = pygame.time.Clock()
        while True:
            self.__screen.fill(Colors.blue)
            self.update_buttons(100, 200, 0, 0, Colors.green, 1)
            self.update_buttons(100, 200, 0, 0, Colors.green, 2)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == MOUSEBUTTONDOWN:
                    if self.__start.hover(pygame.mouse.get_pos()):
                        del self
                        GameScreen().game()
                    if self.__settings.hover(pygame.mouse.get_pos()):
                        del self
                        SettingsScreen().settings()

            if self.__start.hover(pygame.mouse.get_pos()):
                self.update_buttons(120, 220, 10, 10, Colors.yellow, 1)
                time.tick(24)
            if self.__settings.hover(pygame.mouse.get_pos()):
                self.update_buttons(120, 220, 10, 10, Colors.yellow, 2)
                time.tick(24)
            pygame.display.update()