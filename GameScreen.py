__author__ = 'Bohdan'
import pygame
import random
import sys
import os
from Settings import *
from Button import Button

os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()
class GameScreen(object):

    def __init__(self):
        print("GameScreen Start")
        self.__width = 800
        self.__height = 600
        self.__screen = pygame.display.set_mode((self.__width, self.__height))
        pygame.display.set_caption("Snake")
        self.__length = 1
        self.__speed = 15
        self.__points = []
        self.__delta_x = 20
        self.__delta_y = 0
        self.__START = False
        self.__restart = Button()
        reader = open('score.txt','r')
        self.__maxScore = int(reader.read())
        reader.close()
    pass

    def contains(self, points, point):
        xp = point[0]
        yp = point[1]
        for i in points:
            x = i[0]
            y = i[1]
            if abs(x - xp) < 20 or abs(y - yp) < 20:
                return True
        return False

    def apple(self):
        x = random.randint(1, self.__width-20)
        y = random.randint(1, self.__height-20)
        point = (x, y)
        while self.contains(self.__points,(point)) and (x%20 == 0 and y%20 == 0):
            x = random.randint(1, self.__width)
            y = random.randint(1, self.__height)
            point = (x, y)
        return point

    def shift_points(self, points):
        i = points.__len__()-1
        new_points = []
        new_points.append(points[i-1])
        while(i > 1):
            new_points.append(points[i])
            i -= 1
        #return new_points
    pass

    def make_snake(self, arr):
        count = 0
        for i in reversed(arr):
            x = i[0]
            y = i[1]
            if count <= self.__length:
                pygame.draw.rect(self.__screen, Colors.red, (x, y, 20, 20))
            else:
                arr.remove(i)
            count += 1
    pass

    def score(self,flag):
        font = pygame.font.SysFont(None,30)
        s = 'Score: ' + str(self.__length-1)
        if self.__length-1 > self.__maxScore:
            if flag: score = font.render(s+' NEW RECORD!!!', True, Colors.red)
            else: score = font.render(s, True, Colors.red)
        else:score = font.render(s, True, Colors.white)
        self.__screen.blit(score,[10,10])

    def update_buttons(self, btn_width, btn_height, shift_x, shift_y, color):
           self.__restart.create_button(self.__screen,color,
                              self.__width/2-90-shift_x, self.__height/2+50-shift_y,
                              btn_height, btn_width, 0, "Restart", (255, 255, 255),2)

    def game_over_label(self):
        font = pygame.font.SysFont(None,150)
        s = 'GAME OVER'
        self.score(True)
        go = font.render(s, True, Colors.white)
        self.__screen.blit(go,[80, self.__height/2-100])

    def game_over(self):
        self.update_buttons(100,200,0,0,Colors.yellow)
        self.game_over_label()
        pygame.display.update()
        if self.__length > self.__maxScore:
             writer = open('score.txt','w')
             writer.write(str(self.__length-1))
             writer.close()
        time = pygame.time.Clock()
        while True:
            self.__screen.fill(Colors.blue)
            self.game_over_label()
            self.update_buttons(100, 200, 0, 0, Colors.green)
            #self.make_snake(self.__points)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    del self
                    sys.exit(0)
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                          return True
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.__restart.hover(pygame.mouse.get_pos()):
                        print("restart")
                        del self
                        GameScreen().game()
            if self.__restart.hover(pygame.mouse.get_pos()):
                self.update_buttons(120, 220, 10, 10, Colors.yellow)
                time.tick(24)
            pygame.display.update()

    def game (self):
        head_x = int(self.__width/2)
        head_y = int(self.__height/2)
        speed = pygame.time.Clock()

        pygame.draw.rect(self.__screen, Colors.blue, [200,200,20,20])
        self.make_snake(self.__points)
        apple_point = self.apple()
        pygame.draw.rect(self.__screen,Colors.white,apple_point+(20,20))
        pygame.display.update()

        last_presed = pygame.K_RIGHT
        ignore_key = pygame.K_LEFT
        GAME_LIFE = True
        out_screen = False

        while GAME_LIFE:
            for event in pygame.event.get():
                if out_screen: continue
                if event.type == pygame.QUIT:
                    sys.exit(0)
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT and head_x > 0 and ignore_key != pygame.K_LEFT:
                        self.__delta_x = -20
                        ignore_key = pygame.K_RIGHT
                    if event.key == pygame.K_RIGHT and head_x < self.__width and ignore_key != pygame.K_RIGHT:
                        self.__delta_x = +20
                        ignore_key = pygame.K_LEFT
                    if event.key == pygame.K_UP and head_y > 0 and ignore_key != pygame.K_UP:
                        self.__delta_y = -20
                        ignore_key = pygame.K_DOWN
                    if event.key == pygame.K_DOWN and head_y < self.__height and ignore_key != pygame.K_DOWN:
                        self.__delta_y = +20
                        ignore_key = pygame.K_UP
                    last_presed = event.key

            self.__screen.fill(Colors.blue)
            if last_presed == pygame.K_LEFT or last_presed == pygame.K_RIGHT: head_x += self.__delta_x
            else: head_y += self.__delta_y
            out_screen = False

            if self.__points.__contains__((head_x, head_y)):
                  if self.game_over():
                      GameScreen().game()
                      pygame.time.wait(100)
                  else:
                      GAME_LIFE = False
            self.__points.append((head_x, head_y))
            self.shift_points(self.__points)
            uper = False
            if abs(apple_point[0]-head_x) < 20 and abs(apple_point[1]-head_y) < 20:
                self.__length += 1
                apple_point = self.apple()
                uper = True

            pygame.draw.rect(self.__screen,Colors.white, apple_point+(20,20))
            self.make_snake(self.__points)
            self.score(False)
            pygame.display.update()
            if uper and self.__length % 7 == 0 and self.__speed < 24:
                self.__speed+=1

            speed.tick(self.__speed)
            if head_x > self.__width-20:
                head_x = 0
                out_screen =True
            if head_x < 0:
                head_x = self.__width
                out_screen = True
            if head_y > self.__height-20:
                head_y = 0
                out_screen = True
            if head_y < 0:
                head_y = self.__height
                out_screen = True
    pass